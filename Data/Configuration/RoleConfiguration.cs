using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebApp1.Data.Configuration
{

    public class RoleConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
            new IdentityRole
            {
                Name = "Admin",
                NormalizedName = "ADMIN"
            },
            new IdentityRole
            {
                Name = "Sales_Agent",
                NormalizedName = "SALES_AGENT"
            },
            new IdentityRole
            {
                Name = "Affiliate",
                NormalizedName = "AFFILIATE"
            },
            new IdentityRole
            {
                Name = "Vendor",
                NormalizedName = "VENDOR"
            },
            new IdentityRole
            {
                Name = "Franchisee and sales agent of franchisee",
                NormalizedName = "FRANCHISEE AND SALES AGENT OF FRANCHISEE"
            },
            new IdentityRole
            {
                Name = "Mover",
                NormalizedName = "MOVER"
            },
            new IdentityRole
            {
                Name = "Crew_manager",
                NormalizedName = "CREW_MANAGER"
            },
            new IdentityRole
            {
                Name = "Driver",
                NormalizedName = "DRIVER"
            },
            new IdentityRole
            {
                Name = "Developer",
                NormalizedName = "DEVELOPER"
            },
            new IdentityRole
            {
                Name = "Administrative_staff",
                NormalizedName = "ADMINISTRATIVE_STAFF"
            },
            new IdentityRole
            {
                Name = "Costumer_area",
                NormalizedName = "COSTUMER_AREA"
            }
            );
        }
    }
}