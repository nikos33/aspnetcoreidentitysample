﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp1.Data.Migrations
{
    public partial class AddNewRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "23ba45e7-460f-4b9f-83e2-effe0aa5263d", "274fbad4-fabb-492d-830e-44edb1604e84", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e3201935-0707-43de-9bea-914260145fbd", "261a1831-51f2-4bbe-a7c6-ee4d7d0efdfb", "Sales_Agent", "SALES_AGENT" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "23584cd4-a850-4513-9fdb-a7fa69a85273", "77fae2eb-6381-4dcb-a046-a900f34d80af", "Affiliate", "AFFILIATE" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "201c5a50-1e07-4f5b-ba9f-38b6695093c2", "f0666275-adae-4a79-aa88-83186eec4312", "Vendor", "VENDOR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "47370594-34bf-4891-a49e-06abc3c7e517", "ab95fca4-dc3a-46e0-a11b-47d114dbda03", "Franchisee and sales agent of franchisee", "FRANCHISEE AND SALES AGENT OF FRANCHISEE" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "8f624337-4a7d-49fc-8610-d444b3a63129", "3c2e38e7-d4be-4de0-b236-a756e69fff16", "Mover", "MOVER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e2aea4ed-59e0-4f78-80d5-000881e05a82", "f40fcd62-2ea2-4459-a7ee-91646a8154a0", "Crew_manager", "CREW_MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "7e110d73-cebd-404e-8584-f8419d57e77c", "5be1c18e-e24d-4e74-bff3-644e98a2d613", "Driver", "DRIVER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "bd4def51-431c-4c08-873f-5cc1ab76373f", "35d1ebf3-137a-419c-b977-d77c98d52782", "Developer", "DEVELOPER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "18ead1a1-5cd2-4fea-8f43-014c6412596c", "1c8619a4-f633-4910-a67e-dbd01d99e7e7", "Administrative_staff", "ADMINISTRATIVE_STAFF" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "981b93de-c838-43bd-9c32-6d1f0b273e6e", "85ba070e-9049-457b-9d8b-cc28ef732893", "Costumer_area", "COSTUMER_AREA" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "18ead1a1-5cd2-4fea-8f43-014c6412596c");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "201c5a50-1e07-4f5b-ba9f-38b6695093c2");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "23584cd4-a850-4513-9fdb-a7fa69a85273");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "23ba45e7-460f-4b9f-83e2-effe0aa5263d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "47370594-34bf-4891-a49e-06abc3c7e517");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7e110d73-cebd-404e-8584-f8419d57e77c");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8f624337-4a7d-49fc-8610-d444b3a63129");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "981b93de-c838-43bd-9c32-6d1f0b273e6e");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bd4def51-431c-4c08-873f-5cc1ab76373f");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e2aea4ed-59e0-4f78-80d5-000881e05a82");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e3201935-0707-43de-9bea-914260145fbd");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "AspNetUsers");
        }
    }
}
